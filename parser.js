<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-firestore.js"></script>
  <script>
    var firebaseConfig = {
      apiKey: "AIzaSyARN16Ygh9vEbguDw0FXmEfxGqAIL9sr9w",
      authDomain: "oceana-dev.firebaseapp.com",
      databaseURL: "https://oceana-dev.firebaseio.com",
      projectId: "oceana-dev",
      storageBucket: "oceana-dev.appspot.com",
      messagingSenderId: "953188458226",
      appId: "1:953188458226:web:ea3c8d5b4bfa335fb2fa6b",
      measurementId: "G-E4JLE31SH1"
    };
    // Initialize Firebase
    const fb = firebase.initializeApp(firebaseConfig);
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        contactUs.style.display = 'none';
        authNavBarItem.style.display = 'flex';
        socialLoginLink.style.display = 'none';
        avatarLink.style.backgroundImage = "url(" + user.photoURL + ")";
        avatarLink.style.backgroundSize = 'cover';
        plansLink.style.display = 'block';
        var now = new Date().toISOString();
        firebase.firestore().collection("page_views").add({
          name: user.displayName,
          date: now,
          uid: user.uid,
          email: user.email,
          city: localStorage.getItem('city'),
          country: localStorage.getItem('country'),
          country_code: localStorage.getItem('country_code'),
          continent_code: localStorage.getItem('continent_code'),
          device: localStorage.getItem('device'),
          browser: localStorage.getItem('browser'),
          os: localStorage.getItem('OS'),
          page_location: document.getElementsByTagName("title")[0].innerHTML,
        });
      } else {
        // User is signed out.
        authNavBarItem.style.display = 'none';
        plansLink.style.display = 'none';
        contactUs.style.display = 'block';
        socialLoginLink.style.display = 'flex';
      }
    });
    signinButton.addEventListener('click', signup);
    function signup() {
      var provider = new firebase.auth.GoogleAuthProvider();
      firebase.auth().signInWithPopup(provider).then(function (result) {
        var token = result.credential.accessToken;
        var user = result.user;
      }).catch(function (error) {
        console.log(error);
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
      });
    }
    function logout() {
      firebase.auth().signOut().then(function () {
      }).catch(function (error) {
        // An error happened.
      });
    }
    logoutButton.addEventListener('click', logout);


    function getOperatingSystem() {
      var OSName = "Unknown OS";
      if (navigator.userAgent.indexOf("Win") != -1) OSName = "Windows";
      if (navigator.userAgent.indexOf("Mac") != -1) OSName = "Macintosh";
      if (navigator.userAgent.indexOf("Linux") != -1) OSName = "Linux";
      if (navigator.userAgent.indexOf("Android") != -1) OSName = "Android";
      if (navigator.userAgent.indexOf("like Mac") != -1) OSName = "iOS";
      return OSName;
    }

    function getDeviceType() {
      const ua = navigator.userAgent;
      if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        return "tablet";
      }
      if (
        /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
          ua
        )
      ) {
        return "mobile";
      }
      return "desktop";
    };

    function getBrowser() {
      if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1) {
        return 'Opera';
      }
      else if (navigator.userAgent.indexOf("Chrome") != -1) {
        return 'Chrome';
      }
      else if (navigator.userAgent.indexOf("Safari") != -1) {
        return 'Safari';
      }
      else if (navigator.userAgent.indexOf("Firefox") != -1) {
        return 'Firefox';
      }
      else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) //IF IE > 10
      {
        return 'IE';
      }
      else {
        return 'unknown';
      }
    }

    function obtainUserData() {
      var localGeoData = localStorage.getItem('geodata');
      if (localStorage.getItem('geodata') === null) {


        $.getJSON('https://ipapi.co/json/', function (data) {
          console.log(data)
          localStorage.setItem('geodata', JSON.stringify(data));
          localStorage.setItem('city', data.city);
          localStorage.setItem('country', data.country_name);
          localStorage.setItem('country_code', data.country_code);
          localStorage.setItem('continent_code', data.continent_code);
        });
      };
      if (localStorage.getItem('device') === null) {
        localStorage.setItem('device', getDeviceType());
      }
      if (localStorage.getItem('browser') === null) {
        localStorage.setItem('browser', getBrowser());
      }
      if (localStorage.getItem('OS') === null) {
        localStorage.setItem('OS', getOperatingSystem());
      }
    }

    obtainUserData();

  </script>